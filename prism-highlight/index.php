<?php

/*
 * Bail if called directly
 */
defined('ABSPATH') || exit;

/*
 * Enqueue the block's assets for the editor.
 *
 * `wp-blocks`:     includes block type registration and related functions.
 * `wp-element`:    includes the WordPress Element abstraction for
 *                  describing the structure of your blocks.
 * `wp-i18n`:       To internationalize the block's text.
 *
 * @since 1.0.0
 */
function gb_prism_editor_assets()
{
    // Editor Scripts.
    wp_enqueue_script(
        'gb-prism-block-script', // Handle.
        // Block.build.js: We register the block here. Built with Webpack.
        plugins_url('block.build.js', __FILE__),
        array('wp-blocks', 'wp-i18n', 'wp-element'),
        // Dependencies, defined above.
        filemtime(plugin_dir_path(__FILE__) . 'block.js')
        // filemtime — Gets file modification time.
    );

    // Editor Styles.
    wp_enqueue_style(
        'gb-prism-editor-style', // Handle.
        plugins_url('editor.css', __FILE__), // Block editor CSS.
        array('wp-edit-blocks'), // Dependency to include the CSS after it.
        filemtime(plugin_dir_path(__FILE__) . 'editor.css')
        // filemtime — Gets file modification time.
    );

    // Prism Script for Editor
    wp_enqueue_script(
        'gb-prism-highlight-script', // Handle.
        // Block.build.js: We register the block here. Built with Webpack.
        plugins_url('vendor/js/prism.js', __FILE__),
        array('vendor/js/clipboard.js')
    );

    // Prismm Style for Editor
    wp_enqueue_style(
        'gb-prism-styles',
        plugins_url('vendor/css/prism.css', __FILE__),
        array('wp-edit=blocks'),
        filemtime(plugin_dir_path(__FILE__) . 'vendor/css/prism.css')
    );


}

// Hook: Editor assets.
add_action('enqueue_block_editor_assets', 'gb_block_04_tweet_editor_assets');

// Only run this if we're not an ad,om
if (! is_admin()) {
    /**
     * Enqueue the block's assets for the frontend.
     *
     * @since 1.0.0
     */
    function gb_prism_frontend_block_assets()
    {
        // Prism Style for front end
        wp_enqueue_style(
            'gb-prism-styles',
            plugins_url('vendor/css/prism.css', __FILE__),
            array('wp-edit=blocks'),
            filemtime(plugin_dir_path(__FILE__) . 'vendor/css/prism.css')
        );

    }

    // Hook: Frontend assets.
    add_action('enqueue_block_assets', 'gb_prism_frontend_block_assets');
}
