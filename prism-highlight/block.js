const { __ } = wp.i18n;
const { registerBlockType, PlainText, Editable, Children } = wp.blocks;


registerBlockType( 'rivendellweb/prism-highlight', {
	title: __( 'Prism Highlighter' ),
	icon: 'universal-access-alt',
	category: 'layout',
	attributes: {
		language: {
			type: 'string'
		},
		content: {
			type: 'string'
		}
	},

	edit({ className, attributes, setAttributes }) {
		return (
			<pre>
				<PlainText
					className='language-'{attributes.language}
					value={attributes.content}
					onChange={(content) => setAttributes({ content })}
				/>
			</pre>
		);
	},

	save() {
		return <div style={ blockStyle }>Basic example with JSX! (front)</div>;
	},

} );
