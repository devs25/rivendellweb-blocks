<?php

/**
 * Plugin Name: Gutenberg Examples
 * Plugin URI: https://github.com/WordPress/gutenberg-examples
 * Description: This is a plugin demonstrating how to register
 * new blocks for the Gutenberg editor.
 *
 * PHP Version 7
 *
 * @category Gutenberg_Blocks
 * @package  Rivendellweb
 * @author   Carlos Araya <carlos.araya@gmail.com>
 * @license  MIT http: //caraya.mit-license.org/
 * @version  GIT: 9c4f69586deeb695ca2d5828c20159f8fc6b36a1
 * @link     https: //github.com/caraya/rivendellweb-blocks
 * @since    0.1.0
 */

defined('ABSPATH') || exit;

// We use include to avoid fatal errors if the plugin fails to
// load.
include 'prism-highlight/index.php';
